// [SECTION] Dependencies and Modules
	const User = require("../models/User");
	const bcrypt = require("bcrypt");
	const auth = require("../auth");
	const Course = require("../models/Course");

// [SECTION] Functionalities [Create]
	// Register User
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let mobil = data.mobileNo;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10),
			mobileNo: mobil
		});
		
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return false;
			};
		});
	};

	// Email Checker
	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return "Email already exists.";
			} else {
				return "Email is still available.";
			};
		});
	};

	// Login (User Authentication)
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return false;
			} else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPassW, passW);
				if (isMatched) {
					let dataNiUser = result.toObject();
					return {accessToken: auth.createAccessToken(dataNiUser)};
				} else {
					return false;
				};
			};
		});
	};

	// Non-Admin Course Enroll
	module.exports.enroll = async (data) => {
		let id = data.userId;
		let course = data.courseId;

		let isUserUpdated = await User.findById(id).then(user => {
			let listOfEnrollments = [];
			user.enrollments.forEach((element) => listOfEnrollments.push(element.courseId));

			let alreadyEnrolled = 0
			for (let i = 0; i < listOfEnrollments.length; i++) {
				if (listOfEnrollments[i] === course) {
					alreadyEnrolled += 1;
					break;
				} else {
					continue;				
				}
			}

			if (alreadyEnrolled === 1) {
				return false;
			} else {
				user.enrollments.push({courseId: course});
				return user.save().then((saved, err) => {
					if (err) {
						return false;
					} else {
						return true;
					};
				});
			}
		});

		let isCourseUpdated = await Course.findById(course).then(course => {
			let listOfEnrollees = [];
			course.enrollees.forEach((element) => listOfEnrollees.push(element.userId));

			let alreadyEnrolled = 0
			for (let i = 0; i < listOfEnrollees.length; i++) {
				if (listOfEnrollees[i] === id) {
					alreadyEnrolled += 1;
					break;
				} else {
					continue;				
				}
			}

			if (alreadyEnrolled === 1) {
				return false;
			} else {
				course.enrollees.push({userId: id});
				return course.save().then((saved, err) => {
					if (err) {
						return false; 
					} else {
						return true;
					};
				});
			}
		});

		if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return "Enrollment failed. Go to registrar."
		};
	};

// [SECTION] Functionalities [Retrieve]
	// Get User Profile
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};

// [SECTION] Functionalities [Update]
	// Set User as Admin (isAdmin = true)
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		};

		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return "Failed to implement updates.";
			}
		});
	};

	// Set User as Non-Admin (isAdmin = false)
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		};

		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return true;
			} else {
				return "Failed to update user.";
			}
		});
	};