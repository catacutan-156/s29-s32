// [SECTION] Dependencies and Modules
	const Course = require("../models/Course");

// [SECTION] Functionality [Create]
	// Add Course
	module.exports.addCourse = (info) => {
		let course = info.course;

		let cName = course.name;
		let cDesc = course.description;
		let cCost = course.price;

		let newCourse = new Course({
			name: cName,
			description: cDesc, 
			price: cCost
		});

		return newCourse.save().then((savedCourse, err) => {
			if (savedCourse) {
				return savedCourse;
			} else {
				return false;
			}
		});
	};

// [SECTION] Functionality [Retrieve]
	// Retrieve All Courses (Admin)
	module.exports.getAllCourse = () => {
		return Course.find({}).then(result => {
			return result;
		});
	};

	// Retrieve ONLY Active Courses
	module.exports.getAllActive = () => {
		return Course.find({isActive: true}).then(result => {
			return result;
		});
	};

	// Retrieve Single Course
	module.exports.getCourse = (id) => {
		return Course.findById(id).then(result => {
			return result;
		});
	};

// [SECTION] Functionality [Update]
	// Update Course Details
	module.exports.updateCourse = (course, details) => {
		let cName = details.name;
		let cDesc = details.description;
		let cCost = details.price;
		
		let updatedCourse = {	
			name: cName,
			description: cDesc,
			price: cCost
		};

		let id = course.courseId;

		return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
			if (courseUpdated) {
				return true;
			} else {
				return "Failed to update course.";
			}
		});
	};

	// Archive Course
	module.exports.archiveCourse = (course) => {
		let id = course.courseId;
		let updates = {
			isActive: false
		};

		return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
			if (archived) {
				return "Course archived.";
			} else {
				return false;
			}
		});
	};

// [SECTION] Functionality [Delete]
	// Delete Course [Dynamic]
	module.exports.deleteCourse = (courseId) => {
		return Course.findById(courseId).then(course => {
			if (course === null) {
				return "No resource was found.";
			} else {
				return course.remove().then((removedCourse, err) => {
					if (err) {
						return "Failed to remove course.";
					} else {
						return "Successfully destroyed data.";
					}
				});
			};
		});
	};