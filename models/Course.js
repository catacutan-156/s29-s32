// [SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

// [SECTION] Schema
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Name is required."]
		},
		description: {
			type: String,
			required: [true, "Description is required."]
		},
		price: {
			type: Number,
			required: [true, "Price is required."]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "User's ID is required."]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});

// [SECTION] Model
	module.exports = mongoose.model("Course", courseSchema);