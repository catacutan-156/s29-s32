// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require("../controllers/courses");
	const auth = require("../auth");

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] POST Routes
	// Add Course
	route.post("/", auth.verify ,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body
		};

		if (isAdmin) {
			controller.addCourse(data).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send("User unauthorized to proceed.");
		};
	});

// [SECTION] GET Routes
	// Retrieve All Courses
	route.get("/all", auth.verify, (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;

		isAdmin ? controller.getAllCourse().then(outcome => res.send(outcome)) : res.send("You can't do that. Unauthorized.")
	});

	// Retrieve Active Courses
	route.get("/", (req, res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	// Retrieve Single Course
	route.get("/:id", (req, res) => {
		let id = req.params.id
		controller.getCourse(id).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] PUT Routes
	// Update Course
	route.put("/:courseId", auth.verify, (req, res) => {
		let params = req.params;
		let body = req.body;
		if (!auth.decode(req.headers.authorization).isAdmin) {
			res.send("User unauthorized.");
		} else {
			controller.updateCourse(params, body).then(outcome => {
				res.send(outcome);
			});
		};
	});

	// Course Archive
	route.put("/:courseId/archive", auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
		(isAdmin) ? 
			controller.archiveCourse(params).then(outcome => {
				res.send(outcome);
			})
		:
			res.send("Unauthorized user.");
	});

// [SECTION] DEL Routes
	// Delete Course [Dynamic]
	route.delete("/:courseId", auth.verify, (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let id = req.params.courseId

		isAdmin ? controller.deleteCourse(id).then(outcome => res.send(outcome)) : res.send("Not possible, you're not an admin.")
	});

// [SECTION] Export Route System
	module.exports = route;