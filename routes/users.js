// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require("../controllers/users");
	const auth = require("../auth");

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] POST Routes
	// Add User
	route.post("/register", (req, res) => {
		let userData = req.body;
		controller.registerUser(userData).then(outcome => {
			res.send(outcome)
		});
	});

	// Check Email
	route.post("/check-email", (req, res) => {
		controller.checkEmailExists(req.body).then(outcome => {
			res.send(outcome);
		});
	});

	// User Login
	route.post("/login", (req, res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome);
		});
	});

	// Course Enrollment
	route.post("/enroll", auth.verify, (req, res) => {
		let payload = auth.decode(req.headers.authorization);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		let subjectId = req.body.courseId;

		let data = {
			userId: userId,
			courseId: subjectId
		};

		if (!isAdmin) {
			controller.enroll(data).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send("You're an admin, you can't enroll.")
		};
	});

// [SECTION] GET Routes
	// User Profile
	route.get("/details", auth.verify ,(req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] PUT Routes
	// Set User as Admin
	route.put("/:userId/set-as-admin", auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		let id = req.params.userId;

		(isAdmin) ? controller.setAsAdmin(id).then(outcome => res.send(outcome))
		: res.send("User unauthorized.");
	});

	// Set User as Non-Admin
	route.put("/:userId/set-as-user", auth.verify, (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let id = req.params.userId;

		(isAdmin) ? controller.setAsNonAdmin(id).then(outcome => res.send(outcome)) : res.send("You are not authorized.");
	});

// [SECTION] DEL Routes

// [SECTION] Export Route System
	module.exports = route;